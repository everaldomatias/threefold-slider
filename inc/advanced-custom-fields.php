<?php
/**
 * Campos personalizados do plugin Advanced Custom Field
 */
if ( function_exists( 'register_field_group' ) ) {

	// Threefold Slider
	register_field_group( array(
		'id' => 'threefold_slider',
		'title' => 'Links do Slider',
		'fields' => array(
			array(
				'key' => 'field_5b80016528580',
				'label' => 'Habilitar Slider',
				'name' => 'habilitar_slider',
				'type' => 'true_false',
				'instructions' => 'Gerencie se o slider deve ser exibido ou não.',
				'required' => 1,
				'message' => '',
				'default_value' => 1,
			),
			array(
				'key' => 'field_5b80028aa88f0',
				'label' => 'Link Externo ou Personalizado',
				'name' => 'externo_ou_personalizado',
				'type' => 'true_false',
				'instructions' => 'Gerencie se deseja utilizar um link interno ou um link personalizado.',
				'required' => 0,
				'message' => '(Marcado para usar link interno)',
				'default_value' => 1,
			),
			array(
				'key' => 'field_25874128151a8',
				'label' => 'Linkar com um Post',
				'name' => 'link_post',
				'type' => 'post_object',
				'conditional_logic' => array(
					'status' => 1,
					'rules' => array(
						array(
							'field' => 'field_5b80028aa88f0',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'post_type' => array(
					0 => 'post',
				),
				'taxonomy' => array(
					0 => 'all',
				),
				'allow_null' => 1,
				'multiple' => 0,
			),
			array(
				'key' => 'field_5882567d151a9',
				'label' => 'Link externo',
				'name' => 'link_externo',
				'type' => 'text',
				'conditional_logic' => array(
					'status' => 1,
					'rules' => array(
						array(
							'field' => 'field_5b80028aa88f0',
							'operator' => '!=',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'instructions' => 'Preencha esse campo apenas se desejar linkar o slider com um item que não seja Notícias do próprio site.',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'threefold-slider',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array(
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array(
				0 => 'custom_fields',
			),
		),
		'menu_order' => 0,
	));

	// Threefold Slider
	register_field_group( array(
		'id' => 'acf_imagem-do-slider',
		'title' => 'Imagem do Slider',
		'fields' => array(
			array(
				'key' => 'field_58fdff49c04f1',
				'label' => 'Detalhes sobre a imagem',
				'name' => 'slider_image',
				'type' => 'image',
				'instructions' => 'A imagem deve ter as dimensões mínimas de <strong style="color=red">700 x 700px</strong>.
	<br>Imagens menores farão com que o layout fique irregular.
	<br>Já imagens maiores serão cortadas proporcionalmente a partir do centro da imagem.',
				'required' => 1,
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'uploadedTo',
			),
			array(
				'key' => 'field_5b80057c08973',
				'label' => 'Ordem do Slider',
				'name' => 'ordem_slider',
				'type' => 'number',
				'default_value' => '0',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'threefold-slider',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array(
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array(
				0 => 'custom_fields',
				1 => 'featured_image',
			),
		),
		'menu_order' => 0,
	));

}