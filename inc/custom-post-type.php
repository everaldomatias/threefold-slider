<?php
function ts_custom_post_types() {
	/* CPT Slider */
	$labels = array(
		'name'                => _x( 'Threefold Slider Manager', 'Post Type General Name', 'odin' ),
		'singular_name'       => _x( 'Slider2', 'Post Type Singular Name', 'odin' ),
		'menu_name'           => __( 'Threefold Slider', 'odin' ),
		'name_admin_bar'      => __( 'Slider3', 'odin' ),
		'parent_item_colon'   => __( 'Slider Parente', 'odin' ),
		'all_items'           => __( 'Todos Sliders', 'odin' ),
		'add_new_item'        => __( 'Adicionar novo Slide', 'odin' ),
		'add_new'             => __( 'Novo Slider', 'odin' ),
		'new_item'            => __( 'Novo Slider', 'odin' ),
		'edit_item'           => __( 'Editar Slider', 'odin' ),
		'update_item'         => __( 'Atualizar Slider', 'odin' ),
		'view_item'           => __( 'Ver Slider', 'odin' ),
		'search_items'        => __( 'Buscar Slider', 'odin' ),
		'not_found'           => __( 'Não encontrado', 'odin' ),
		'not_found_in_trash'  => __( 'Não encontrado na lixeira', 'odin' ),
	);
	$args = array(
		'label'               => __( 'slider', 'odin' ),
		'description'         => __( 'Slides', 'odin' ),
		'labels'              => $labels,
		'supports'            => array( 'title' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-slides',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'capability_type'     => 'page',
	);
	register_post_type( 'threefold-slider', $args );
}
