jQuery(document).ready(function($) {
	var w = jQuery(document).width();
	if ( w <= 768 ) {
		$( '.threefold-slider' ).slick({
			adaptiveHeight: true,
			arrows: false,
			dots: false,
			fade: true,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true
		});
	}
});