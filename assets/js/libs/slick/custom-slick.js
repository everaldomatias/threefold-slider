jQuery(document).ready(function($) {
    $('.slick-slider').slick({
        slidesToShow: 6,
        slidesToScroll: 6,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 3000,
        responsive: [
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
    });
});