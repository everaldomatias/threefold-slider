<?php
/**
 * Plugin Name:       Threefold Slider
 * Description:       Plugin de slider com até três itens por visualizção.
 * Plugin URI:        https://github.com/everaldomatias/threefold-slider
 * Version:           0.0.1
 * Author:            Everaldo Matias
 * Author URI:        https://everaldomatias.gitlab.io
 * Requires at least: 4.0.0
 * Tested up to:      4.9.0
 *
 * @package Threefold_Slider
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * 
 * Adiciona o Custom Post Type do Slider.
 *
 * @see	inc/advanced-custom-fields.php
 * @link https://codex.wordpress.org/Post_Types
 * 
 */
if ( ! function_exists( 'ts_custom_post_types' ) ) {
	require_once 'inc/custom-post-type.php';
}

/**
 * 
 * Verifica se o plugin Advanced Custom Field está
 * ativo e então carrega os campos personalizados.
 * 
 * @see	inc/advanced-custom-fields.php
 * 
 */
if ( class_exists( 'acf' ) ) {
	require_once 'inc/advanced-custom-fields.php';
}

/**
 * Main Threefold_Slider Class
 *
 * @class Threefold_Slider
 * @version	0.0.1
 * @since 0.0.1
 * @package	Threefold_Slider
 */

if ( ! class_exists( 'Threefold_Slider' ) ) {
	final class Threefold_Slider {

		/**
		 * Configura o plugin
		 */
		public function __construct() {
			add_action( 'init', array( $this, 'threefold_slider_plugin_setup' ), -1 );
			add_action( 'init', 'ts_custom_post_types', 0 );

			//$this->ts_include();
			//add_action( 'init', array( 'Threefold_Slider_Front_End', 'get_instance' ), 0 );
			add_shortcode( 'threefold-slider', array( $this, 'ts_shortcode' ) );
		}

		/**
		 * Configura tudo que for necessário para o plugin
		 */
		public function threefold_slider_plugin_setup() {
			add_action( 'wp_enqueue_scripts', array( $this, 'threefold_slider_plugin_js' ), 999 );
			add_action( 'wp_enqueue_scripts', array( $this, 'threefold_slider_plugin_css' ), 999 );
		}

		/**
		 * Inclui os arquivos necessários para o plugin.
		 */
		public function ts_include() {
			require_once 'inc/ts-front-end.php';
		}

		/**
		 * Adiciona o .css na fila
		 *
		 * @return void
		 */
		public function threefold_slider_plugin_css() {
			wp_enqueue_style( 	'threefold_slider_plugin', plugins_url( '/assets/css/threefold-slider.css', __FILE__ ) );
			wp_enqueue_style( 	'slick', plugins_url( '/assets/js/libs/slick/slick.css', __FILE__ ) );
		}

		/**
		 * Adiciona o .js na fila
		 *
		 * @return void
		 */
		public function threefold_slider_plugin_js() {
			wp_enqueue_script( 	'slick', plugins_url( '/assets/js/libs/slick/slick.min.js', __FILE__ ), array(), '1.5.6', true );
			wp_enqueue_script( 	'threefold_slider_plugin', plugins_url( '/assets/js/threefold-slider.js', __FILE__ ) );
		}

		/**
		 * Adiciona suporte para tradução.
		 */
		public function threefold_slider_plugin_textdomain() {
		    load_plugin_textdomain( 'threefold-slider', false, basename( dirname( __FILE__ ) ) . '/languages/' );
		}

		public function ts_shortcode() {

			$args = array(
				'post_type'			=> 'threefold-slider',
				'posts_per_page'	=> 3,
				'post_status'		=> 'publish',
				'meta_key'			=> 'ordem_slider',
				'order'				=> 'ASC',
				'orderby'			=> 'meta_value',
			);
			$threefold_slider = new WP_Query( $args );

			if ( $threefold_slider->have_posts() ) :
				echo '<div class="threefold-slider">';

					while ( $threefold_slider->have_posts() ) : $threefold_slider->the_post();

						$threefold_slider_image = get_field( 'slider_image' );

						if ( ! empty( $threefold_slider_image ) ) :

							if ( $threefold_slider->post_count == 3 ) {
								$class = 'col-sm-4';
								$image_src = wp_get_attachment_image_src( $threefold_slider_image, array( '700', '700' ) );
							} elseif ( $threefold_slider->post_count == 2 ) {
								$class = 'col-sm-6';
								$image_src = wp_get_attachment_image_src( $threefold_slider_image, array( '700', '700' ) );
							} elseif ( $threefold_slider->post_count == 1 ) {
								$class = 'col-sm-12 full-slider';
								$image_src = wp_get_attachment_image_src( $threefold_slider_image, array( '1350', '700' ) );
							}

							echo '<div class="' . $class .' nopadding each">';

								$externo_ou_personalizado = get_field( 'externo_ou_personalizado' );
								
								if ( $externo_ou_personalizado && $link = get_field( 'link_post' ) ) :
									echo '<a href="' . esc_url( get_the_permalink( $link->ID ) ) . '">';
										echo '<img src="' . esc_url( $image_src[0] ) . '" alt="' . esc_html( get_the_title() ) . '">';
										echo '<div class="desc">';
											echo '<span class="categoria">';
												$categories = get_the_category( $link->ID );
												echo esc_html( $categories[0]->name );
											echo "</span><!-- categoria -->";
											echo '<span class="content">' .  esc_html( get_the_title() ) . '</span>';
										echo '</div><!-- .desc -->';
									echo '</a>';
								elseif ( ! $externo_ou_personalizado && $link_externo = get_field( 'link_externo' ) ) :
									echo '<a href="' . esc_url( $link_externo ) . '">';
										echo '<img src="' . esc_url( $image_src[0] ) . '" alt="' . esc_html( get_the_title() ) . '">';
										echo '<div class="desc">';
											echo '<span class="content">' .  esc_html( get_the_title() ) . '</span>';
										echo '</div><!-- .desc -->';
									echo '</a>';
								else :
									echo '<img src="' . esc_url( $image_src[0] ) . '" alt="' . esc_html( get_the_title() ) . '">';
									echo '<div class="desc">';
										echo '<span class="content">' .  esc_html( get_the_title() ) . '</span>';
									echo '</div><!-- .desc -->';
								endif;
							
							echo '</div><!-- .each -->';

						endif;

					endwhile;

				echo '</div><!-- .threefold-slider -->';
			endif;

		}

	} // Threefold_Slider
} // Endif

/**
 * Instancia a classe do plugin
 *
 * @return void
 */
function threefold_slider_plugin_main() {
	new Threefold_Slider();
}

/**
 * Inicializa o plugin
 */
add_action( 'plugins_loaded', 'threefold_slider_plugin_main' );